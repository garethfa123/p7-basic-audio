//
//  SinOscillator.hpp
//  JuceBasicAudio
//
//  Created by Gareth on 13/11/2017.
//
//

#ifndef SinOscillator_hpp
#define SinOscillator_hpp

#include <stdio.h>
#include "../../JuceLibraryCode/JuceHeader.h"


class SinOscillator
{
public:
    /** Constructor */
    SinOscillator();
    
    /** Destructor */
    ~SinOscillator();
    
    int getSample()
    {
        
    }
    
    void setFrequency()
    {
        
    }
    
    void setAmplitude()
    {
        
    }
private:
    Atomic<float> gain;
    Atomic<float> frequency;
    float phasePosition;
    float sampleRate;
};

#endif /* SinOscillator_hpp */
